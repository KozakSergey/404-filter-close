<?php
/*Часто требуется закрыть от индексации несуществующие страницы фильтра.

Если у вас задача по типу:
"Для несуществующих страниц фильтра типа '/catalog/ochki_i_opravy/filter/brand_ref-is-sdfgsdfgsdfg/apply/' устанавливать meta robots noindex,nofollow или каноникал",
то это решение для вас.

В component_epilog.php шаблона компонента catalog.smart.filter нужно вставить код*/
function find_key_value($array, $key, $val)
{
    foreach ($array as $item)
    {
        if (is_array($item) && find_key_value($item, $key, $val)) return true;

        if (isset($item[$key]) && $item[$key] == $val) return true;
    }

    return false;
}

if(preg_match("/\/filter\//", $_SERVER['REQUEST_URI']))
{
    $issetFilterUrl = find_key_value($arResult,"FILTER_URL",$_SERVER['REQUEST_URI']);
  
    if($issetFilterUrl===false && preg_match("/\/filter\/clear\/apply\//", $arResult["FILTER_URL"]))
    {
        // добавим метатэг роботс на страницу
        $APPLICATION->AddHeadString('<meta name="robots" content="noindex, nofollow" />');
        
        // если нужно установить статус 404
        http_response_code(404);
        CHTTP::SetStatus("404 Not Found");
        @define("ERROR_404","Y");
    }
}
?>