# Закрытие несуществующих страниц умного фильтра


Часто требуется закрыть от индексации несуществующие страницы фильтра.

Если у вас задача по типу:
"Для несуществующих страниц фильтра типа '/catalog/ochki_i_opravy/filter/brand_ref-is-sdfgsdfgsdfg/apply/' устанавливать meta robots noindex,nofollow или каноникал",
то это решение для вас.

В component_epilog.php шаблона компонента catalog.smart.filter нужно вставить код